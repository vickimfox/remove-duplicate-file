# remove-duplicate-file

Sample Java application that looks for duplicate files. It starts with a Keep directory and then looks at one or more Duplicate directories. Duplicate files found in the Duplicate directories are deleted.