package com.vickifox.remove_duplicate_file;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;

/**
 * The Main entry point for the application.
 * <p>
 * Due to limited use, this Main has the hard-coded values that applies at
 * runtime.
 */
public class Main {

	private final DupFileStruct dfs = new DupFileStruct();

	/**
	 * The main entry point.
	 * 
	 * @param args Are ignored.
	 */
	public static void main(String[] args) {
		if (args.length > 0) {
			System.out.println("*INFO* Arguments passed to program are ignored.");
		}

		Main m = new Main();
		try {
			m.run();
			System.out.println("*INFO* Program ended successfully");
			
		} catch (Throwable t) {
			System.out.println("*ERROR* Unexpected error. " + t.toString());
			t.printStackTrace();
		}
	}

	private void run() throws Exception {
		@SuppressWarnings("resource")
		FileSystem fs = FileSystems.getDefault();  // close() not supported on default file system
		
		this.dfs.loadKeepDirectory(fs.getPath("D:", "Media", "Artwork", "Artists"));
		
		this.dfs.loadDupDirectory(fs.getPath("D:", "Archives", "Workarea", "Employment", "IBMWork"));

		
		int totalDeleted = this.dfs.deleteDuplicates();
		
		System.out.println("Number of file entries: " + this.dfs.size());
		System.out.println("Number deleted: " + totalDeleted);
	}
}
