/**
 * 
 */
package com.vickifox.remove_duplicate_file;

import java.util.Comparator;
import java.util.Objects;

/**
 * File Identity
 * <p>
 * The unique name and size combination that identifies a unique file.
 */
public class FileIdent implements Comparator<FileIdent> {

	private final String name;
	
	private final Long size;
	
	private static final FileIdent NULL_INSTANCE = new FileIdent();
	
	
	/**
	 * Create instance.
	 * 
	 * @param aInstance
	 */
	public FileIdent(InstancePath aInstance) {
		this.name = aInstance.getName();
		this.size = Long.valueOf(aInstance.getSize());
	}
	
	private FileIdent() {
		this.name = "";
		this.size = Long.valueOf(0);
	}
	
	/**
	 * @return string
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return long
	 */
	public long getSize() {
		return this.size.longValue();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.name, this.size);
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result;
		if (this == obj) {
			result = true;
		} else {
			if (obj instanceof FileIdent) {
				FileIdent other = (FileIdent) obj;
				result = Objects.equals(this.name.toUpperCase(), other.name.toUpperCase());
				result &= Objects.equals(this.size, other.size);
			} else {
				result = false;
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		return PseudoJsonBuilder.createObject("FileIdent")
				.addElement("name", this.name)
				.addElement("size", this.size)
				.build();
	}
	
	@Override
	public int compare(FileIdent aInstance1, FileIdent aInstance2) {
		int result;
		if (aInstance1 == null) {
			if (aInstance2 == null) {
				result = 0;
			} else {
				result = 1;
			}
		} else {
			if (aInstance2 == null) {
				result = -1;
			} else {
				result = aInstance1.name.compareToIgnoreCase(aInstance2.name);
				if (result == 0) {
					result = aInstance1.size.compareTo(aInstance2.size);
				}
			}
		}
		return result;
	}
	
	/**
	 * @return comparator
	 */
	public static Comparator<FileIdent> getComparator() {
		return NULL_INSTANCE;
	}
}
