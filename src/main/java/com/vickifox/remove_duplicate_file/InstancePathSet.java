/**
 * 
 */
package com.vickifox.remove_duplicate_file;

import java.util.Set;
import java.util.TreeSet;

/**
 * Instance Path Set
 * <p>
 * A collection of instance path elements. Using a set to ensure uniqueness.
 */
public class InstancePathSet {
	
	private InstancePath keepInstance = null;

	private Set<InstancePath> duplicateInstances = new TreeSet<>(InstancePath.getComparator());
	
	
	/**
	 * Create instance path set.
	 */
	public InstancePathSet() {
		super();
	}

	/**
	 * Set keep instance.
	 * 
	 * @param aInstance
	 */
	public void setKeepInstance(InstancePath aInstance) {
		this.keepInstance = aInstance;
	}
	
	/**
	 * Is keep instance empty?
	 * 
	 * @return boolean
	 */
	public boolean isKeepInstanceEmpty() {
		return (this.keepInstance == null);
	}
	
	/**
	 * Add duplicate instance.
	 * 
	 * @param aInstance
	 */
	public void addDuplicateInstance(InstancePath aInstance) {
		this.duplicateInstances.add(aInstance);
	}
	
	/**
	 * Delete duplicate instances. 
	 * @return int
	 */
	public int deleteDups() {
		int nbrDeleted = 0;
		if (! isKeepInstanceEmpty()) {
			for (InstancePath ip : this.duplicateInstances) {
				ip.delete();
				nbrDeleted ++;
			}
		}
		return nbrDeleted;
	}
}
