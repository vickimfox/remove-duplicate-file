/**
 * 
 */
package com.vickifox.remove_duplicate_file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Duplicate File Structure
 * <p>
 * This structure associates a file with all instances in the loaded directories.
 */
public class DupFileStruct {
	
	private final SortedMap<FileIdent, InstancePathSet> fileToDirs = new TreeMap<>(FileIdent.getComparator());

	/**
	 * Load Keep Directory entries.
	 * 
	 * @param aDir
	 */
	public void loadKeepDirectory(Path aDir) {
		try {
			InstanceVisitor iv = new InstanceVisitor(this.fileToDirs, true);
			Files.walkFileTree(aDir, iv);
			
		} catch (IOException e) {
			System.out.printf("*ERROR* Problem processing keep directory. %s :: %s%n", e.getLocalizedMessage(), aDir);
		}
	}

	/**
	 * Load Duplicate Directory entries.
	 * 
	 * @param aDir
	 */
	public void loadDupDirectory(Path aDir) {
		try {
			InstanceVisitor iv = new InstanceVisitor(this.fileToDirs, false);
			Files.walkFileTree(aDir, iv);
			
		} catch (IOException e) {
			System.out.printf("*ERROR* Problem processing duplicate directory. %s :: %s%n", e.getLocalizedMessage(), aDir);
		}
	}

	/**
	 * Delete Duplicate Directory entries.
	 * 
	 * @return int
	 */
	public int deleteDuplicates() {
		int nbrDeleted = 0;
		for (Map.Entry<FileIdent, InstancePathSet> entry : this.fileToDirs.entrySet()) {
			nbrDeleted += entry.getValue().deleteDups();
		}
		return nbrDeleted;
	}
	
	/**
	 * Return size.
	 * 
	 * @return int
	 */
	public int size() {
		return this.fileToDirs.size();
	}
}
