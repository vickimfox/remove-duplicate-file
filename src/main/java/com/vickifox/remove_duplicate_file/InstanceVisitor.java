/**
 * 
 */
package com.vickifox.remove_duplicate_file;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.SortedMap;

/**
 * Instance Visitor.
 * <p>
 * Version of FileVisitor for the file instances.
 */
public class InstanceVisitor implements FileVisitor<Path> {

	private final SortedMap<FileIdent, InstancePathSet> fileToDirs;

	private final boolean keepDir;

	/**
	 * Create instance for specific map and directory type.
	 * 
	 * @param aFileToDirs
	 * @param aKeepDir
	 */
	public InstanceVisitor(SortedMap<FileIdent, InstancePathSet> aFileToDirs, boolean aKeepDir) {
		this.fileToDirs = aFileToDirs;
		this.keepDir = aKeepDir;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		System.out.printf("Visiting dir :: %s%n", dir.toString());
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		InstancePath instance = new InstancePath(file, attrs);
		if (instance.isFile()) {
			FileIdent ident = new FileIdent(instance);

			InstancePathSet pathSet = this.fileToDirs.get(ident);
			if (pathSet == null) {
				pathSet = new InstancePathSet();
				this.fileToDirs.put(ident, pathSet);
			}
			if (this.keepDir) {
				pathSet.setKeepInstance(instance);
			} else {
				pathSet.addDuplicateInstance(instance);
			}
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		if (exc != null) {
			System.out.printf("*WARN* Problem working with file. Ignored. %s :: %s%n", exc.getLocalizedMessage(),
					file.toString());
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		if (exc != null) {
			System.out.printf("*WARN* Problem processing directory. Ignored. %s :: %s%n", exc.getLocalizedMessage(),
					dir.toString());
		}
		return FileVisitResult.CONTINUE;
	}

}
