/**
 * 
 */
package com.vickifox.remove_duplicate_file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Instance Path
 * <p>
 * Stores information about the directory where the file instance was found.
 */
public class InstancePath implements Comparator<InstancePath> {

	private final Path filePath;

	private final FileTime addDate;

	private final FileTime modDate;

	private final Boolean regularFile;

	private final Long size;

	private static final InstancePath NULL_INSTANCE = new InstancePath();

	/**
	 * Create Instance Path for the specified file instance.
	 * 
	 * @param aFileInstance File instance
	 * @param aAttributes File basic attributes
	 */
	public InstancePath(Path aFileInstance, BasicFileAttributes aAttributes) {
		this.filePath = aFileInstance;
		this.addDate = aAttributes.creationTime();
		this.modDate = aAttributes.lastModifiedTime();
		this.regularFile = Boolean.valueOf(aAttributes.isRegularFile());
		this.size = Long.valueOf(aAttributes.size());
	}

	private InstancePath() {
		this.filePath = Path.of("dev", "null");
		this.addDate = FileTime.from(0L, TimeUnit.SECONDS);
		this.modDate = FileTime.from(0L, TimeUnit.SECONDS);
		this.regularFile = Boolean.FALSE;
		this.size = Long.valueOf(0);
	}

	/**
	 * Get name.
	 * 
	 * @return String
	 */
	public String getName() {
		return this.filePath.getFileName().toString();
	}

	/**
	 * Get parent path.
	 * 
	 * @return Path
	 */
	public Path getParentPath() {
		return this.filePath.getParent();
	}

	/**
	 * Get add date.
	 * 
	 * @return LocalDate
	 */
	public FileTime getAddDate() {
		return this.addDate;
	}

	/**
	 * Get modify date.
	 * 
	 * @return LocalDate
	 */
	public FileTime getModDate() {
		return this.modDate;
	}

	/**
	 * Is regular file?
	 * 
	 * @return boolean
	 */
	public boolean isFile() {
		return this.regularFile.booleanValue();
	}

	/**
	 * Get size.
	 * 
	 * @return long
	 */
	public long getSize() {
		return this.size.longValue();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.filePath);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof InstancePath))
			return false;
		InstancePath other = (InstancePath) obj;
		return Objects.equals(this.filePath, other.filePath);
	}

	@Override
	public String toString() {
		//Formatter:off
		return PseudoJsonBuilder.createObject("InstancePath")
				.addElement("filePath", this.filePath)
				.addElement("regularFile", this.regularFile)
				.addElement("size", this.size)
				.build();
		//Formatter:on
	}

	/**
	 * Delete file.
	 */
	public void delete() {
		try {
			DosFileAttributes dosAttr = Files.readAttributes(this.filePath, DosFileAttributes.class);
			if (dosAttr.isReadOnly()) {
				Files.setAttribute(this.filePath, "dos:readonly", Boolean.FALSE);
			}

		    Files.delete(this.filePath);
		    System.out.println("Delete " + toString());
		} catch (IOException e) {
		   System.out.printf("*WARN* Problem deleting file. %s :: %s%n", e.toString(), this.toString());
		}
	}

	@Override
	public int compare(InstancePath aInstance1, InstancePath aInstance2) {
		if (aInstance1 == null) {
			return 1;
		}
		if (aInstance2 == null) {
			return -1;
		}
		return aInstance1.filePath.compareTo(aInstance2.filePath);
	}

	/**
	 * Get comparator.
	 * 
	 * @return comparator
	 */
	public static Comparator<InstancePath> getComparator() {
		return NULL_INSTANCE;
	}
}
