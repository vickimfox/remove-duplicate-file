/**
 * 
 */
package com.vickifox.remove_duplicate_file;

import java.util.Objects;

/**
 * Pseudo-JSON Builder
 * <p>
 * Very specialized builder for toString() implementations.
 */
public class PseudoJsonBuilder {

	private final StringBuilder sb = new StringBuilder();
	
	private boolean addComma = false;
	
	private PseudoJsonBuilder(String aObjectName) {
		this.sb.append( quoteString(aObjectName) );
		this.sb.append(": {");
	}
	
	/**
	 * @param aObjectName
	 * @return PseudoJsonBuilder
	 */
	public static PseudoJsonBuilder createObject(String aObjectName) {
		return new PseudoJsonBuilder(aObjectName);
	}
	
	/**
	 * @param aKey
	 * @param aValue
	 * @return PseudoJsonBuilder
	 */
	public PseudoJsonBuilder addElement(String aKey, Object aValue) {
		if (this.addComma) {
			this.sb.append(",");
		} else {
			this.addComma = true;
		}
		this.sb.append(" ");
		this.sb.append(quoteString(aKey));
		this.sb.append(": ");
		this.sb.append(quoteString(Objects.toString(aValue)));
		return this;
	}
	
	/**
	 * @return String
	 */
	public String build() {
		this.sb.append(" }");
		return this.sb.toString();
	}
	
	
	private static String quoteString(String aString) {
		return "\"" + aString + "\"";
	}
}
